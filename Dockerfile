FROM alpine:latest

MAINTAINER nick@nickrosener.co

RUN apk add --update mysql mysql-client && rm -f /var/cache/apk/*

CMD trap : TERM INT; (while true; do sleep 1000; done) & wait